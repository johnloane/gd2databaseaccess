package Daos;
/* Declares the methods that all userDAO types must implement
they could be mySQL, Mongo, Postgres

Classes in my Business Layer should use a reference to the interface type
to avoid dependencies on the underlying concrete classes
 */

import DTOs.User;
import Exceptions.DaoException;

import java.util.List;

public interface IUserDaoInterface
{
    public List<User> findAllUsers() throws DaoException;
    public User findUserByUsernamePassword(String username, String password) throws DaoException;
}
