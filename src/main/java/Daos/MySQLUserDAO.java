package Daos;

import DTOs.User;
import Exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLUserDAO extends MySqlDao implements IUserDaoInterface
{

    @Override
    public List<User> findAllUsers() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();

        try
        {
            con = this.getConnection();

            String query = "select * from user";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                int userId = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("Password");
                String lastname = rs.getString("last_name");
                String firstname = rs.getString("first_name");
                User u = new User(userId, firstname, lastname, username, password);
                users.add(u);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findAllUsers() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
        return users;
    }

    @Override
    public User findUserByUsernamePassword(String uname, String pword) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        try
        {
            con = this.getConnection();

            String query = "select * from user where username = ? and password = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, uname);
            ps.setString(2, pword);

            rs = ps.executeQuery();

            if(rs.next())
            {
                int userId = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("Password");
                String lastname = rs.getString("last_name");
                String firstname = rs.getString("first_name");
                u = new User(userId, firstname, lastname, username, password);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findUserByUsernamePassword() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }

        }
        return u;
    }
}
