package BusinessObjects;

import DTOs.User;
import Daos.*;
import Exceptions.DaoException;

import java.util.List;
import java.util.logging.Level;

public class MainApp
{
    public static void main(String[] args)
    {
        IUserDaoInterface IUserDao = new MySQLUserDAO();
        //MongoDAO test = new MongoDAO();
        //test.getConnection();
        /* Notice that the userDao reference is an Interface type
            This will allow for different concrete implementations
            We could change to Mongo without changing anything in the
            interface
            If the interface doesn't change then none of the code below
            needs to change.
            The contract defined by the interface will not be broken.
            This means that this code is independent of the code used
            to access the database. (Reduced coupling)

            The Business Objects require that all User DAOs implement
            the IUserDAOInterface as the code uses only references to
            the interface
         */

        try
        {
            List<User> users = IUserDao.findAllUsers();

            if(users.isEmpty())
            {
                System.out.println("There are no users");
            }

            for(User user : users)
            {
                System.out.println("User: " + user.toString());
            }

            //test dao with good username and password
            User user = IUserDao.findUserByUsernamePassword("smithj", "password");
            if(user != null)
            {
                System.out.println("User found: " + user);
            }
            else
            {
                System.out.println("No user with that username and password");
            }

            user = IUserDao.findUserByUsernamePassword("madmax", "password");
            if(user != null)
            {
                System.out.println("User found: " + user);
            }
            else
            {
                System.out.println("No user with that username and password");
            }

            //test dao with bad username and password
        }
        catch(DaoException e)
        {
            e.getMessage();
        }
    }
}
